from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.f1, name = 'home'),
    path('any',views.f2, name= 'second'),
]