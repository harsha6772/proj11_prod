from django.shortcuts import render
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from json import dumps

# Create your views here.
def f1(request):
    return render(request,'index1.html')

def f2(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['doc']
        fs = FileSystemStorage()
        fs.save(uploaded_file.name,uploaded_file)

    names = {
        'file_name' : uploaded_file.name,
    }
    namesJSON = dumps(names)
    return render(request,'index2.html', {'data':namesJSON})
