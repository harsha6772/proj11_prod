var mymap;
var satellite;
var osm;
var basemaps;
var drawnItems
$(document).ready(function() {

    mymap = L.map('map_id',{
        center:[52.572, 4.917],
        zoom:12,
        minZoom:2,
        maxZoom: 18,
        layersControl:true,
        //drawControl: true,
    });

    osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap) //same as "mymap.addLayer(osm)"

    satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 21,
        id: 'mapbox.satellite',
        accessToken: 'sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg'
    }).addTo(mymap);

    /////////////////////////////////
    /*
    var grid;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'static/media_dup/ndvi_grid_31UFU_22.09.2020.geojson');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.responseType = 'json';
    xhr.onload = function() {
        if (xhr.status !== 200) return
        L.geoJSON(xhr.response).addTo(mymap);
    };
    xhr.send();
    */
    /////////////////////////////////////
    
    var bare;
    var grid = fetch('/static/media_dup/ndvi_grid.geojson')
        .then(function(resp){
            return resp.json();
        })
        .then(function(abc){
            L.geoJSON(abc).addTo(mymap);
            //bare = data.features[0].properties.bare_Soil
            //return data;
        });
      
    ////////////////////////////////    
    /*
    var ndvi = fetch('/static/media_dup/ndvi_proj.tif')
    .then(function(resp){
        return resp.arrayBuffer();
    })
    .then(function(data){
        var s = L.ScalarField.fromGeoTIFF(data);
        let layer = L.canvasLayer.scalarField(s).addTo(mymap);
        //L.geoJSON(data).addTo(mymap);
        //bare = data.features[0].properties.bare_Soil
        //return data;
    });
    */
       
    //////////////////////////////////////////////////////////////////////////////////
    
    var ndvi = fetch('/static/media_dup/ndvi_proj.tif')
    .then(function(resp){
        return resp.arrayBuffer();
    })
    .then(function(abc){
        var tiff = GeoTIFF.parse(abc);
        var image = tiff.getImage();
        var tiffWidth = image.getWidth();
        var tiffHeight = image.getHeight();
        var rasters = image.readRasters();
        var tiepoint = image.getTiePoints()[0];
        var pixelScale = image.getFileDirectory().ModelPixelScale;
        var geoTransform = [tiepoint.x, pixelScale[0], 0, tiepoint.y, 0, -1*pixelScale[1]];
        
        

        ////////
        var ndviData = new Array(tiffHeight);
        for (var j = 0; j<tiffHeight; j++){
            ndviData[j] = new Array(tiffWidth);
            for (var i = 0; i<tiffWidth; i++){
                ndviData[j][i] = rasters[0][i + j*tiffWidth];
            }
        }
        

        ////////
        /*
        var cs_def = {positions:[0.0,0.030303030303,0.0606060606061,0.0909090909091,0.121212121212,0.151515151515,0.181818181818,0.212121212121,0.242424242424,0.272727272727,0.30303030303,0.333333333333,0.363636363636,0.393939393939,0.424242424242,0.454545454545,0.484848484848,0.515151515152,0.545454545455,0.575757575758,0.606060606061,0.636363636364,0.666666666667,0.69696969697,0.727272727273,0.757575757576,0.787878787879,0.818181818182,0.848484848485,0.878787878788,0.909090909091,0.939393939394,0.969696969697,1.0],
        colors:["#ffffff", "#e5e5e6" , "#d1d1d1", "#bababa", "#979797", "#646464",
                    "#1464d3", "#1e6eeb", "#2883f1", "#3c97f5", "#50a5f5", "#78b9fb", "#97d3fb", "#b5f1fb", "#e1ffff",
                    "#0ea10e", "#1eb31e", "#36d33c", "#50ef50", "#78f572", "#97f58d", "#b5fbab", "#c9ffbf",
                    "#ffe978", "#ffc13c", "#ffa100", "#ff6000", "#ff3200", "#e11400", "#c10000", "#a50000",
                    "#643c32", "#785046", "#8d645a"]};
        */
        
        var cs_def = {positions:[0, 0.2, 0.4, 0.6, 1], colors:["white","red", "brown", "yellow", "green"]};
        
        var scaleWidth = 256;
        var canvasColorScale = document.createElement('canvas');
        canvasColorScale.width = scaleWidth;
        canvasColorScale.height = 1;
        canvasColorScale.style.display = "none";

        document.body.appendChild(canvasColorScale);

        var contextColorScale = canvasColorScale.getContext("2d");
        var gradient = contextColorScale.createLinearGradient(0, 0, scaleWidth, 1);

        ////////
        for (var i = 0; i < cs_def.colors.length; ++i) {
            gradient.addColorStop(cs_def.positions[i], cs_def.colors[i]);
        }
        contextColorScale.fillStyle = gradient;
        contextColorScale.fillRect(0, 0, scaleWidth, 1);

        ////////
        var csImageData = contextColorScale.getImageData(0, 0, scaleWidth-1, 1).data;

        ////////
        var width = 680;
        var height = 500;

        var canvasRaster = document.createElement('canvas');
        canvasRaster.width = width;
        canvasRaster.height = height;
        canvasRaster.style.display = "none";

        document.body.appendChild(canvasRaster);
        

        var contextRaster = canvasRaster.getContext("2d");
        console.log(contextRaster)

        var id = contextRaster.createImageData(width,height);//
        var data = id.data;
        var pos = 0;
        var invGeoTransform = [-geoTransform[0]/geoTransform[1], 1/geoTransform[1],0,-geoTransform[3]/geoTransform[5],0,1/geoTransform[5]];
            for(var j = 0; j<height; j++){
                for(var i = 0; i<width; i++){
                    var pointCoordsX = geoTransform[0] + i*tiffWidth*geoTransform[1]/width;
                    var pointCoordsY = geoTransform[3] + j*tiffHeight*geoTransform[5]/height;
                    var px = Math.round(invGeoTransform[0] + pointCoordsX * invGeoTransform[1]);
                    var py = Math.round(invGeoTransform[3] + pointCoordsY * invGeoTransform[5]);

                    var value;
                    if(Math.floor(px) >= 0 && Math.ceil(px) < image.getWidth() && Math.floor(py) >= 0 && Math.ceil(py) < image.getHeight()){
                        var value = ndviData[py][px];

                        var c = Math.round((scaleWidth-1) * (value-0.1)/0.9);
                        var alpha = 200;
                        if (c<0 || c > (scaleWidth-1)){
                        alpha = 0;
                        }
                        data[pos]   =   csImageData[c*4];;
                        data[pos+1]   = csImageData[c*4+1];
                        data[pos+2]   = csImageData[c*4+2];
                        data[pos+3]   = alpha;
                        pos = pos + 4
                }
                }
            }
        /////////
        contextRaster.putImageData( id, 0, 0);
        var imageBounds = [[geoTransform[3], geoTransform[0]], [geoTransform[3] + tiffHeight*geoTransform[5], geoTransform[0] + tiffWidth*geoTransform[1]]];

        var imageLayer = L.imageOverlay(canvasRaster.toDataURL(), imageBounds,{
            opacity: 1
          }).addTo(mymap);

    });


    ////////////////////////////////////
    basemaps = {
        "OSM":osm,
        "Satellite" : satellite,
    };

    
    drawnItems  = L.featureGroup().addTo(mymap);

    var drawControlFull = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },

        draw: {
            polyline:false,
            marker :false,
            circle:false,
            polygon : {
                allowIntersection:true,
                showArea:true,
                tap:false,
                shapeOptions: {
                    stroke: false,
                    color: 'red',
                    weight: 7,
                    opacity: 0.5,
                    fill: true,
                    fillColor: 'yellow', //same as color by default
                    fillOpacity: 0.4,
                    clickable: true
                }
            },
            rectangle : {
                showArea:true
            }
        }
    });

    var drawControlEditOnly = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        },
        draw: false
    });


    mymap.addControl(drawControlFull);

    L.control.layers(basemaps,{'drawLayer':drawnItems}).addTo(mymap);

    var jsonObj
    mymap.on('draw:created', function(e){
        var layer = e.layer;
        drawnItems.addLayer(layer);
        geoJSON_obj = layer.toGeoJSON();
        jsonObj = JSON.stringify(geoJSON_obj);
        $('#user_aoi').val(jsonObj);
        drawControlFull.remove(mymap);
        drawControlEditOnly.addTo(mymap);
    });

    mymap.on('draw:deleted', function(e) {
        drawControlEditOnly.remove(mymap);
        drawControlFull.addTo(mymap);
     });

    $('#sub_id').click(function(){
        var indice_val = $('#indice_list').val()
    })
    

    function latlong_string(ll) {
        return "[" + ll.lat.toFixed(3)+ ", "+ ll.lng.toFixed(3)+"]";
    };


    mymap.on('mousemove',function(e) {
        $('#mouse_loc').html(latlong_string(e.latlng))
    });

    


    //L.geoJSON('ndvi_grid_31UFU_22.09.2020.geojson').addTo(mymap);
})