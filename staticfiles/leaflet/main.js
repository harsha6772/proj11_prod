var mymap;
var satellite;
var osm;
var basemaps;
var drawnItems
$(document).ready(function() {

    mymap = L.map('map_id',{
        center:[52.572, 4.917],
        zoom:12,
        minZoom:2,
        maxZoom: 18,
        layersControl:true,
        //drawControl: true,
    });

    osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap) //same as "mymap.addLayer(osm)"

    satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 21,
        id: 'mapbox.satellite',
        accessToken: 'sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg'
    }).addTo(mymap);

    imageBounds = [[40.712216, -74.22655], [40.773941, -74.12544]];

    var grid = fetch('/media/' + data1.file_name)
    .then(function(resp){
        return resp.json();
    })
    .then(function(abc){
        L.geoJSON(abc).addTo(mymap);
        //bare = data.features[0].properties.bare_Soil
        //return data;
    });
  
})